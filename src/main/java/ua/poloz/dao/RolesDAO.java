package ua.poloz.dao;

import ua.poloz.domain.Developer;
import ua.poloz.domain.Roles;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 12.07.13
 * Time: 22:10
 * To change this template use File | Settings | File Templates.
 */
public interface RolesDAO {
    public void addRole(String role);

    public void removeRoles(Integer id);

    public List<Roles> listRoles();
    public List<Developer> listDevelopers();
}

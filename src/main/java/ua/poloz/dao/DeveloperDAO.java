package ua.poloz.dao;

import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: APolozov
 * Date: 25.06.13
 * Time: 17:47
 * To change this template use File | Settings | File Templates.
 */
public interface DeveloperDAO {
    public void addDeveloper(Developer developer);
    public void removeDeveloper(Long id);
    public void editDeveloper(Long id);
    public List<Developer> listDeveloper();
    public List<Project> listProject();
    public List<DeveloperProject> listDevProj();
    public List<Report> reports();
}

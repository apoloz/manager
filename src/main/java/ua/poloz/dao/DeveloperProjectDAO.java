package ua.poloz.dao;

import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 20.07.13
 * Time: 12:14
 * To change this template use File | Settings | File Templates.
 */
public interface DeveloperProjectDAO {
    public List<Developer> listDeveloper();
    public List<Project> listProject();
    public void add(DeveloperProject devProj);
    public List<DeveloperProject> listDevProj();
}

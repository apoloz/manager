package ua.poloz.dao;

import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 20.07.13
 * Time: 12:13
 * To change this template use File | Settings | File Templates.
 */
public interface ProjectDAO {
    public void add(Project project);
    public void editProject(Long id);
    public List<Project> listProject();
    public List<Report> reports();
    public List<DeveloperProject> listDevProj();
}

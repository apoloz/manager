package ua.poloz.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ua.poloz.domain.Developer;
import ua.poloz.domain.Project;
import ua.poloz.domain.Roles;
import ua.poloz.service.DeveloperService;
import ua.poloz.service.ProjectService;
import ua.poloz.service.RolesService;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: APolozov
 * Date: 01.07.13
 * Time: 17:33
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class DeveloperController {
    @Autowired
    private DeveloperService developerService;
    @Autowired
    private RolesService rolesService;

    @RequestMapping("/index")
    public String listDeveloper (Map<String,Object> map){
        map.put("developer", new Developer());
        map.put("listDevelopers", developerService.listDeveloper());
        map.put("roles", new Roles());
        map.put("listRoles", rolesService.listRoles());
//        map.put("login", developerService.login())
        return "developer";
    }

    @RequestMapping("/")
    public String home() {
        return "redirect:/index";
    }

    @RequestMapping(value = "/addDeveloper", method = RequestMethod.POST)
    public String addDeveloper(@ModelAttribute("Developer") Developer developer,
                               BindingResult result){
        developerService.addDeveloper(developer);
        return "redirect:/index";
    }

    @RequestMapping("/delete/{developerId}")
    public String deleteDeveloper(@PathVariable("developerId") Long id){
        developerService.removeDeveloper(id);
        return "redirect:/index";
    }

}

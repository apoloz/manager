package ua.poloz.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import ua.poloz.domain.Project;
import ua.poloz.service.ProjectService;

import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 21.07.13
 * Time: 0:45
 * To change this template use File | Settings | File Templates.
 */
@Controller
public class ProjectController {
    @Autowired
    private ProjectService projectService;

    @RequestMapping("/projects")
    public String listProject(Map<String,Object> map){
          map.put("project", new Project());
        map.put("listProject",projectService.listProject());
        return "projects";
    }
}

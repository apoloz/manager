package ua.poloz.domain;

import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 18.07.13
 * Time: 20:35
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Project")
public class Project {
    private String id;
    private String name;
    private Date startDate;
    private Integer duration;
    private Long idHead;
    private Integer budget;
    private Date projectClosed;
    private Developer head;
    private List<Report> reports;
    private List<DeveloperProject> listDevProj;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "StartDate")
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @Column(name = "Duration")
    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    @Column(name = "id_head")
    public Long getIdHead() {
        return idHead;
    }

    public void setIdHead(Long idHead) {
        this.idHead = idHead;
    }

    @Column(name = "Budget")
    public Integer getBudget() {
        return budget;
    }

    public void setBudget(Integer budget) {
        this.budget = budget;
    }

    @Column(name = "projectClosed")
    public Date getProjectClosed() {
        return projectClosed;
    }

    public void setProjectClosed(Date projectClosed) {
        this.projectClosed = projectClosed;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_head", referencedColumnName = "id", insertable = false, updatable = false)
    public Developer getHead() {
        return head;
    }

    public void setHead(Developer head) {
        this.head = head;
    }

    @OneToMany(mappedBy = "project")
    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    @OneToMany(mappedBy = "project")
    public List<DeveloperProject> getListDevProj() {
        return listDevProj;
    }

    public void setListDevProj(List<DeveloperProject> listDevProj) {
        this.listDevProj = listDevProj;
    }
}

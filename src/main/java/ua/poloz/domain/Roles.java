package ua.poloz.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 13.07.13
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Roles")
public class Roles {

    private Integer id;
    private String nameRole;
    private List<Developer> listDevelopers;

    @Column(name = "name_role")
    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }

    @Id
    @Column(name = "id")
    @GeneratedValue
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @OneToMany(mappedBy = "roles")
    public List<Developer> getListDevelopers() {
        return listDevelopers;
    }

    public void setListDevelopers(List<Developer> listDevelopers) {
        this.listDevelopers = listDevelopers;
    }
}

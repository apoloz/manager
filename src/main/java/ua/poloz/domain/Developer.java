package ua.poloz.domain;

import javax.persistence.*;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 13.07.13
 * Time: 14:09
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("ALL")
@Entity
@Table(name = "Developer")
public class Developer {

    private Long id;
    private String name;
    private String surname;
    private Integer role;
    private String pass;
    private Roles roles;
    private List<Report> reports;
    private List<Project> listProject;
    private List<DeveloperProject> listDevProj;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Column(name = "PASSWORD")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "SURNAME")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "ROLE")
    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ROLE", referencedColumnName = "id", insertable = false, updatable = false)
    public Roles getRoles() {
        return roles;
    }

    public void setRoles(Roles roles) {
        this.roles = roles;
    }

    @OneToMany(mappedBy = "head")
    public List<Project> getListProject() {
        return listProject;
    }

    public void setListProject(List<Project> listProject) {
        this.listProject = listProject;
    }

    @OneToMany(mappedBy = "developer")
    public List<Report> getReports() {
        return reports;
    }

    public void setReports(List<Report> reports) {
        this.reports = reports;
    }

    @OneToMany(mappedBy = "developer")
    public List<DeveloperProject> getListDevProj() {
        return listDevProj;
    }

    public void setListDevProj(List<DeveloperProject> listDevProj) {
        this.listDevProj = listDevProj;
    }
}

package ua.poloz.domain;

import javax.persistence.*;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 18.07.13
 * Time: 20:37
 * To change this template use File | Settings | File Templates.
 */
@Entity
@Table(name = "Dev-Proj")
public class DeveloperProject {
    private String id;
    private Long idDeveloper;
    private Long idProject;
    private Integer participation;
    private Developer developer;
    private Project project;

    @Id
    @Column(name = "id")
    @GeneratedValue
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "participation")
    public Integer getParticipation() {
        return participation;
    }

    public void setParticipation(Integer participation) {
        this.participation = participation;
    }

    @Column(name = "id_developer")
    public Long getIdDeveloper() {
        return idDeveloper;
    }

    public void setIdDeveloper(Long idDeveloper) {
        this.idDeveloper = idDeveloper;
    }

    @Column(name = "id_project")
    public Long getIdProject() {
        return idProject;
    }

    public void setIdProject(Long idProject) {
        this.idProject = idProject;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_developer", referencedColumnName = "id", insertable = false, updatable = false)
    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_project", referencedColumnName = "id", insertable = false, updatable = false)
    public Project getProject() {
        return project;
    }

    public void setProject(Project project) {
        this.project = project;
    }
}

package ua.poloz.service;

import ua.poloz.domain.Developer;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 25.07.13
 * Time: 0:32
 * To change this template use File | Settings | File Templates.
 */
public interface ReportService {
    public List<Developer> listDeveloper();
    public List<Project> listProject();
    public void add(Report report);
    public List<Report> listReport();
}

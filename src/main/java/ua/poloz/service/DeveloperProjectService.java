package ua.poloz.service;

import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 25.07.13
 * Time: 0:32
 * To change this template use File | Settings | File Templates.
 */
public interface DeveloperProjectService {
    public List<Developer> listDeveloper();
    public List<Project> listProject();
    public void add(DeveloperProject devProj);
    public List<DeveloperProject> listDevProj();
}

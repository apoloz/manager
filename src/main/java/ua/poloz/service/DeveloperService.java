package ua.poloz.service;

import org.springframework.transaction.annotation.Transactional;
import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: APolozov
 * Date: 26.06.13
 * Time: 17:38
 * To change this template use File | Settings | File Templates.
 */
public interface DeveloperService {
    public void addDeveloper(Developer developer);
    public void removeDeveloper(Long id);
    public void editDeveloper(Long id);
    public List<Developer> listDeveloper();
    public List<Project> listProject();
    public List<DeveloperProject> listDevProj();
    public List<Report> reports();
}

package ua.poloz.service;

import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 21.07.13
 * Time: 0:02
 * To change this template use File | Settings | File Templates.
 */
public interface ProjectService {
    public void add(Project project);
    public void editProject(Long id);
    public List<Project> listProject();
    public List<Report> reports();
    public List<DeveloperProject> listDevProj();
}

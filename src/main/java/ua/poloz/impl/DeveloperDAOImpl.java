package ua.poloz.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import ua.poloz.dao.DeveloperDAO;
import ua.poloz.domain.*;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: APolozov
 * Date: 25.06.13
 * Time: 17:51
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DeveloperDAOImpl implements DeveloperDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addDeveloper(Developer developer) {
        sessionFactory.getCurrentSession().save(developer);
    }

    @Override
    public void removeDeveloper(Long id) {
        Developer developer = (Developer) sessionFactory.getCurrentSession().load(Developer.class, id);
        if (null != developer) {
            sessionFactory.getCurrentSession().delete(developer);
        }
    }

    @Override
    public void editDeveloper(Long id) {
        Developer developer = (Developer) sessionFactory.getCurrentSession().load(Developer.class, id);
        sessionFactory.getCurrentSession().update(developer);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Developer> listDeveloper() {

        return sessionFactory.getCurrentSession().createQuery("from Developer").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Project> listProject() {
        return sessionFactory.getCurrentSession().createQuery("from Project").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DeveloperProject> listDevProj() {
        return sessionFactory.getCurrentSession().createQuery("from DeveloperProject").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Report> reports() {
        return sessionFactory.getCurrentSession().createQuery("from Report").list();
    }
}

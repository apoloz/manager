package ua.poloz.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.dao.ReportDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 30.07.13
 * Time: 23:49
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ReportDAOImpl implements ReportDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Developer> listDeveloper() {
        return sessionFactory.getCurrentSession().createQuery("from Developer").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Project> listProject() {
        return sessionFactory.getCurrentSession().createQuery("from Project ").list();
    }

    @Override
    public void add(Report report) {
        sessionFactory.getCurrentSession().save(report);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Report> listReport() {
        return sessionFactory.getCurrentSession().createQuery("from Report").list();
    }
}

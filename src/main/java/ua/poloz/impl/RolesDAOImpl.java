package ua.poloz.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.dao.RolesDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.Roles;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 12.07.13
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class RolesDAOImpl implements RolesDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addRole(String role) {
        sessionFactory.getCurrentSession().save(role);
    }

    @Override
    public void removeRoles(Integer id) {
        Roles roles = (Roles) sessionFactory.getCurrentSession().load(Roles.class, id);
        if (null != roles) {
            sessionFactory.getCurrentSession().delete(roles);
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Roles> listRoles() {
        return sessionFactory.getCurrentSession().createQuery("from Roles").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Developer> listDevelopers() {
        return sessionFactory.getCurrentSession().createQuery("from Developer").list();
    }
}

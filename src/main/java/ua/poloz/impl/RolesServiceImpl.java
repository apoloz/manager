package ua.poloz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ua.poloz.dao.RolesDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.Roles;
import ua.poloz.service.RolesService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 15.07.13
 * Time: 1:05
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class RolesServiceImpl implements RolesService {

    @Autowired
    private RolesDAO rolesDAO;

    @Transactional
    @Override
    public void addRole(String role) {
        rolesDAO.addRole(role);
    }

    @Transactional
    @Override
    public void removeRoles(Integer id) {
        rolesDAO.removeRoles(id);
            }

    @Transactional
    @Override
    public List<Roles> listRoles() {
        return rolesDAO.listRoles();
    }

    @Transactional
    @Override
    public List<Developer> listDevelopers() {
        return rolesDAO.listDevelopers();
    }
}

package ua.poloz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.poloz.dao.DeveloperDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;
import ua.poloz.service.DeveloperService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: APolozov
 * Date: 26.06.13
 * Time: 17:43
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DeveloperServiceImpl implements DeveloperService {

    @Autowired
    private DeveloperDAO developerDAO;

    @Transactional
    @Override
    public void addDeveloper(Developer developer) {
        developerDAO.addDeveloper(developer);
    }

    @Transactional
    @Override
    public void removeDeveloper(Long id) {
        developerDAO.removeDeveloper(id);
    }

    @Transactional
    @Override
    public void editDeveloper(Long id) {
        developerDAO.editDeveloper(id);
    }

    @Transactional
    @Override
    public List<Developer> listDeveloper() {
        return developerDAO.listDeveloper();
    }

    @Transactional
    @Override
    public List<Project> listProject() {
        return developerDAO.listProject();
    }

    @Transactional
    @Override
    public List<DeveloperProject> listDevProj() {
        return developerDAO.listDevProj();
    }

    @Transactional
    @Override
    public List<Report> reports() {
        return developerDAO.reports();
    }
}
package ua.poloz.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.dao.ProjectDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 21.07.13
 * Time: 0:10
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class ProjectDAOImpl implements ProjectDAO {

    @Autowired
    private SessionFactory sessionFactory;


    @Override
    public void add(Project project) {
        sessionFactory.getCurrentSession().save(project);
    }

    @Override
    public void editProject(Long id) {
        Project project = (Project) sessionFactory.getCurrentSession().load(Project.class, id);
        sessionFactory.getCurrentSession().update(project);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Project> listProject() {
        return sessionFactory.getCurrentSession().createQuery("from Project").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Report> reports() {
        return sessionFactory.getCurrentSession().createQuery("from Report").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DeveloperProject> listDevProj() {
        return sessionFactory.getCurrentSession().createQuery("from DeveloperProject").list();
    }
}

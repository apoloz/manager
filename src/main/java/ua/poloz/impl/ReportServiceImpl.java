package ua.poloz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.poloz.dao.ReportDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;
import ua.poloz.service.ReportService;

import java.util.Date;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 30.07.13
 * Time: 23:54
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ReportServiceImpl implements ReportService{

    @Autowired
    private ReportDAO reportDAO;

    @Transactional
    @Override
    public List<Developer> listDeveloper() {
        return reportDAO.listDeveloper();
    }

    @Transactional
    @Override
    public List<Project> listProject() {
        return reportDAO.listProject();
    }

    @Transactional
    @Override
    public void add(Report report) {
        reportDAO.add(report);
    }

    @Transactional
    @Override
    public List<Report> listReport() {
        return reportDAO.listReport();
    }
}

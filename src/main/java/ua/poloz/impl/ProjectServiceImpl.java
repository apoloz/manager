package ua.poloz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.poloz.dao.ProjectDAO;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.domain.Report;
import ua.poloz.service.ProjectService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 21.07.13
 * Time: 0:04
 * To change this template use File | Settings | File Templates.
 */
@Service
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    private ProjectDAO projectDAO;

    @Transactional
    @Override
    public void add(Project project) {
       projectDAO.add(project);
    }

    @Transactional
    @Override
    public void editProject(Long id) {
        projectDAO.editProject(id);
    }

    @Transactional
    @Override
    public List<Project> listProject() {
        return projectDAO.listProject();
    }

    @Transactional
    @Override
    public List<Report> reports() {
        return projectDAO.reports();
    }

    @Transactional
    @Override
    public List<DeveloperProject> listDevProj() {
        return projectDAO.listDevProj();
    }
}

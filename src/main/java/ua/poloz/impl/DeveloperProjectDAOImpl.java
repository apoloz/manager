package ua.poloz.impl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ua.poloz.dao.DeveloperProjectDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 30.07.13
 * Time: 7:54
 * To change this template use File | Settings | File Templates.
 */
@Repository
public class DeveloperProjectDAOImpl implements DeveloperProjectDAO{

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Override
    public List<Developer> listDeveloper() {
        return sessionFactory.getCurrentSession().createQuery("from Developer").list();
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Project> listProject() {
        return sessionFactory.getCurrentSession().createQuery("from Project").list();
    }

    @Override
    public void add(DeveloperProject devProj) {
        sessionFactory.getCurrentSession().save(devProj);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<DeveloperProject> listDevProj() {
        return sessionFactory.getCurrentSession().createQuery("from DeveloperProject").list();
    }
}

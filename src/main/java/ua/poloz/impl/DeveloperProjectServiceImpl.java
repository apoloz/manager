package ua.poloz.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.poloz.dao.DeveloperProjectDAO;
import ua.poloz.domain.Developer;
import ua.poloz.domain.DeveloperProject;
import ua.poloz.domain.Project;
import ua.poloz.service.DeveloperProjectService;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: poloz
 * Date: 30.07.13
 * Time: 7:55
 * To change this template use File | Settings | File Templates.
 */
@Service
public class DeveloperProjectServiceImpl implements DeveloperProjectService {

    @Autowired
    private DeveloperProjectDAO developerProjectDAO;

    @Transactional
    @Override
    public List<Developer> listDeveloper() {
        return developerProjectDAO.listDeveloper();
    }

    @Transactional
    @Override
    public List<Project> listProject() {
        return developerProjectDAO.listProject();
    }

    @Transactional
    @Override
    public void add(DeveloperProject devProj) {
        developerProjectDAO.add(devProj);
    }

    @Transactional
    @Override
    public List<DeveloperProject> listDevProj() {
        return developerProjectDAO.listDevProj();
    }
}

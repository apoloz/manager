-- phpMyAdmin SQL Dump
-- version 3.4.11.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Апр 18 2013 г., 23:04
-- Версия сервера: 5.5.29
-- Версия PHP: 5.4.6-1ubuntu1.2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `FinalTest`
--

-- --------------------------------------------------------

-- --------------------------------------------------------

--
-- Структура таблицы `Developer`
--

CREATE TABLE IF NOT EXISTS `test`.`Developer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `surname` varchar(250) NOT NULL,
  `role` int(20) NOT NULL,
  `password` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `role` (`role`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=0 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Project`
--

CREATE TABLE IF NOT EXISTS `Project` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(205) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `StartDate` date DEFAULT NULL,
  `Duration` int(11) DEFAULT NULL,
  `id_head` bigint(20) DEFAULT NULL,
  `Budget` int(11) DEFAULT NULL,
  `projectClosed` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id_head` (`id_head`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Dev-Proj`
--

CREATE TABLE IF NOT EXISTS `Dev-Proj` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_developer` bigint(20) NOT NULL,
  `id_project` bigint(20) NOT NULL,
  `participation` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_developer` (`id_developer`),
  KEY `id_project` (`id_project`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=33 ;

--
-- Структура таблицы `Report`
--

CREATE TABLE IF NOT EXISTS `Report` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `dataReport` date NOT NULL,
  `id_developer` bigint(20) NOT NULL,
  `id_project` bigint(20) NOT NULL,
  `time` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_developer` (`id_developer`),
  KEY `id_project` (`id_project`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `Roles`
--

CREATE TABLE IF NOT EXISTS `Roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_role` varchar(50) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

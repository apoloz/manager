INSERT INTO `test`.`Developer` (id, name, surname, role, password) VALUES
(1, 'Ivan', 'Ivanov', 3, 'qwer'),
(2, 'Wiil', 'Smit', 2, 'Smit'),
(3, 'Petr', 'Petrov', 1, 'Petrov'),
(4, 'Mark', 'Buch', 3, 'Buch'),
(5, 'Herbert', 'Schildt', 2, 'Schildt'),
(6, 'Kathy', 'Sierra', 3, 'Sierra'),
(7, 'Bert', 'Bates', 3, 'Bates'),
(8, 'Barry', 'Burd', 3, 'Burd'),
(9, 'Gary', 'Cornell', 2, 'Cornell'),
(10, 'Jakob', 'Jenkov', 4, 'Jenkov');


<%--
  Created by IntelliJ IDEA.
  User: poloz
  Date: 21.07.13
  Time: 0:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title><spring:message code="label.title" /></title>
</head>
<body>
<form:form method="POST" action="add" commandName="developer">

    <table>
        <tr>
            <td><form:label path="name" >
                <spring:message code="label.name" />
            </form:label></td>
            <td><form:input path="name" required="true"/></td>
        </tr>
        <tr>
            <td><form:label path="surname">
                <spring:message code="label.surname" />
            </form:label></td>
            <td><form:input path="surname" /></td>
        </tr>
        <tr>
            <td><form:label path="id">
                <spring:message code="label.id" />
            </form:label></td>
            <td><form:input path="id" /></td>
        </tr>
        <tr>
            <td><form:label path="role">
                <spring:message code="label.role" />
            </form:label></td>
            <td><form:input path="role" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit"
                                   value="<spring:message code="label.addDeveloper"/>" /></td>
        </tr>
    </table>
</form:form>
</body>
</html>
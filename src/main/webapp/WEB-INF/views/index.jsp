<%--
  Created by IntelliJ IDEA.
  User: poloz
  Date: 21.07.13
  Time: 0:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<ui:composition template="templates/page-template.xhtml"
                xmlns:ui="http://java.sun.com/jsf/facelets"
                xmlns:f="http://java.sun.com/jsf/core"
                xmlns:h="http://java.sun.com/jsf/html">
    <ui:define name="content">
    <ui:define name="content">
<body>
<a href="projects"><spring:message code="label.project" /></a>
<a href="developers"><spring:message code="label.project" /></a>
<a href="report"><spring:message code="label.project" /></a>
<a href="roles"><spring:message code="label.project" /></a>
<a href="statistics"><spring:message code="label.project" /></a>
</body>
        </ui:define>
    </ui:composition>
</html>
<%--
  Created by IntelliJ IDEA.
  User: poloz
  Date: 21.07.13
  Time: 0:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title><spring:message code="label.title" /></title>
    <!--form:label path=""    -->
</head>
<body>

<a href="<c:url value="/logout" />">
<spring:message code="label.logout" />
</a>

<h2><spring:message code="label.title" /></h2>

<form:form method="POST" action="add" commandName="developer">

    <table>
        <tr>
            <td><form:label path="name" >
                <spring:message code="label.name" />
            </form:label></td>
            <td><form:input path="name" required="true"/></td>
        </tr>
        <tr>
            <td><form:label path="surname">
                <spring:message code="label.surname" />
            </form:label></td>
            <td><form:input path="surname" /></td>
        </tr>
        <tr>
            <td><form:label path="role">
                <spring:message code="label.role" />
            </form:label></td>
            <td><form:input path="role" /></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit"
                                   value="<spring:message code="label.addDeveloper"/>" /></td>
        </tr>
    </table>
</form:form>
<a href="projects"><spring:message code="label.project" /></a>

<h3><spring:message code="label.developers" /></h3>
<c:if test="${!empty listDevelopers}">
    <table class="data">
        <tr>
            <th><spring:message code="label.id" /></th>
            <th><spring:message code="label.name" /></th>
            <th><spring:message code="label.role" /></th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${listDevelopers}" var="developer">
            <tr>
                <td>${developer.id}</td>
                <td>${developer.name}, ${developer.surname}</td>
                <td>${developer.roles.nameRole}</td>
                <td><a href="delete/${developer.id}"><spring:message code="label.delete" /></a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>

<h3><spring:message code="label.role" /></h3>
<c:if test="${!empty listRoles}">
    <table class="data">
        <tr>
            <th><spring:message code="label.id" /></th>
            <th><spring:message code="label.name" /></th>

            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${listRoles}" var="roles">
            <tr>
                <td>${roles.id}</td>
                <td>${roles.nameRole}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: poloz
  Date: 21.07.13
  Time: 0:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf8">
    <title><spring:message code="label.title" /></title>
</head>
<body>
<h3><spring:message code="label.project"/></h3>
<c:if test="${!empty listProject}">
    <table class="data" border="1">
        <tr>
            <td><spring:message code="label.projectName"/></td>
            <td><spring:message code="label.head"/></td>
            <td><spring:message code="label.start"/></td>
            <td><spring:message code="label.budget"/></td>
            <td><spring:message code="label.duration"/></td>
            <td><spring:message code="label.end"/></td>
        </tr>
        <c:forEach items="${listProject}" var="project">
            <tr>
                <td>${project.name}</td>
                <td>${project.head.name}</td>
                <td>${project.startDate}</td>
                <td>${project.budget}</td>
                <td>${project.duration}</td>
                <td>${project.projectClosed}</td>
            </tr>
        </c:forEach>
    </table>
</c:if>
<a href="index"><spring:message code="label.menu" /></a>
</body>
</html>